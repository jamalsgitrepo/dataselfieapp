                                // ALL THE SERVER SIDE CODE GOES INTO THIS FILE //
// Comment
// Have acess to the EXPRESS node package
const express = require ('express');
const Datastore = require('nedb');
// Create an app variable and assign the express library to it so we can use the express functions
const app = express();

// Enter port for the web server to listen
const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Listening at http://localhost:${port}`));

// Enter the path to the index.html file or folder
app.use(express.static('public'));
app.use(express.json({ limit: '1mb' }));
//
const database  = new Datastore('database.db');
database.loadDatabase();

app.get('/api', (request, response) => {
    database.find({}, (err, data) => {
        if (err) {
            response.end();
        }
        response.json(data);
    });

});

app.post('/api', ( request, response ) => {
    console.log('I got a request');
    const data = request.body;
    const timestamp = Date.now();
    data.timestamp = timestamp;
    database.insert(data);
    response.json(data);
});